<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> COW. </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/fun.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-expand-md navbar-light sticky-top">
    <a href="hw.blade.php" class="navbar-brand"> COW. </a>
    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarMenu">
        <ul class="navbar-nav text-black-50">
            <li class="nav-item active">
                <a href="{{ route('hw') }}" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('fun') }}" class="nav-link">Fun</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('about') }}" class="nav-link">About</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('signup') }}" class="nav-link">Sign Up</a>
            </li>
        </ul>
    </div>
    <form class="form-inline pr-2">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
</nav>

<div class="jumbotron mb-0" style="background-image: url('{{asset('images/penny.jo.jpg')}}');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
            </div>
            <div class="col-6 text-secondary text-right pr-0" style="font-family: 'Playfair Display', serif; font-size: 1.9vw;">
                <p>Drunken rural carousers swear by their stories of tipping over cows in the middle of the night, but most experts assert that there's more urban myth going on than actual tipping. A 2005 study at the University of British Columbia concluded that tipping a cow would require an exertion of 2,910 newtons of force; meaning that a 4'7" cow pushed at an angle of 23.4 degrees relative to the ground would require the equivalent strength of 4.43 people to tip the poor thing over.</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid px-0 bg-secondary text-gray-50">
    <div class="jumbotron">
        <p class="text-center" style="font-size: 2.2vw;">Like humans, cows form close friendships and choose to spend much of their time with 2-4 preferred individuals. They also hold grudges for years and may dislike particular individuals.</p>
    </div>
</div>


<div class="container-fluid p-4">
    <div class="row">
        <div class="col-4 "  style="font-family: font-family: 'Heebo', sans-serif; font-size: 1.9vw;">
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4715.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">What is it called when a cow blends in with his surroundings?
                <br>Being CaMOOflauged.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4719.jpg')}}">
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4721.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">WWhat magazine makes cows stampede to the news stand?
                <br>Cows-mopolitan.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4725.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">What cows say during sex?<br>They mooaning.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4729.jpg')}}">
        </div>
        <div class="col-4" style="font-family: font-family: 'Heebo', sans-serif; font-size: 1.9vw;">
            <p class="text-center p-3 border border-secondary text-dark">How do you get a cow to keep quiet?
                <br>Press the moooote button.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4716.jpg')}}">
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4720.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">Where do cows go on Saturday nights?
                <br>To the MOOO-vies.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4722.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">What do you say to a cow that crosses in front of your car?
                <br>Mooo-ve over.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4728.jpg')}}">
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4727.jpg')}}">
        </div>
        <div class="col-4"  style="font-family: font-family: 'Heebo', sans-serif; font-size: 1.9vw;">
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4717.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">What was the cows favorite part of math?<br>Moo-ltiplication.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4718.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">What are grumpy cows called?
                <br>Moo-dy.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4723.jpg')}}">
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4724.jpg')}}">
            <p class="text-center p-3 border border-secondary text-dark">What happens when a cow is exhausted?
                <br>It cowlapses.</p>
            <img class="img-fluid pb-3" src="{{asset('images/IMG_4730.jpg')}}">
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
