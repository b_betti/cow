<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> COW. </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/about.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">


</head>
<body class="body">
<nav class="navbar navbar-expand-md sticky-top">
    <a href={{asset('hw')}} class="navbar-brand text-body"> COW. </a>
    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarMenu">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a href="{{ route('hw') }}" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('fun') }}" class="nav-link">Fun</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('about') }}" class="nav-link">About</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('signup') }}" class="nav-link">Sign Up</a>
            </li>
        </ul>
    </div>
    <form class="form-inline pr-2">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
</nav>

<section>
    <div class="container-fluid" id="plx" style="background-image: url('{{asset('images/boci_prx.jpg')}}')">
        <div class="row">
            <div class="wtitle text-center">
                <h2 class="text-white display-2">ABOUT</h2>
                <h4 class="text-white-50">Cows are as diverse as cats, dogs, and humans: Some are very quick learners, while others are a little slower. Some are bold and adventurous, while others are shy and timid. Some are friendly and considerate, while others are bossy and devious.</h4>
            </div>
        </div>
    </div>
</section>



<div class="story container-fluid bg-light">
    <div class="row text-center">
        <div class="col m-4 bg-white py-3">
            <div class="card card-cascade wider">
                <div class="view view-cascade overlay">
                    <div class="card-img-wrap"><img class="card-img-top" src="{{asset('images/boci_card.jpg')}}"></div>
                    <a href="#!">
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class="card-body card-body-cascade text-center">
                    <h4 class="card-title"><strong>COW</strong></h4>
                    <h5 class="blue-text pb-2"><strong>Bos taurus</strong></h5>
                    <p class="card-text">Cattle - colloquially cows - are the most common type of large domesticated ungulates. They are a prominent modern member of the subfamily Bovinae, are the most widespread species of the genus Bos, and are most commonly classified collectively as Bos taurus.</p>
                </div>
            </div>
            <div id="demo" class="carousel slide py-4" data-ride="carousel">

                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{asset('images/ab.1.jpg')}}" alt="ab.1">
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('images/ab.2.jpg')}}" alt="ab.2">
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('images/ab.3.jpg')}}" alt="ab.3">
                    </div>
                </div>

                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
        </div>

        <div class="col-md-6 bg-white m-4">
            <div class="arc" style="display: inline-block">
                <div class="arct-1">
                    <img class="rounded p-4 float-left" src="{{asset('images/bocikk.jpg')}}">
                    <p class="arct-text text-right p-4">According to research, cows are generally quite intelligent animals who can remember things for a long time. Animal behaviorists have found that they interact in socially complex ways, developing friendships over time and sometimes holding grudges against other cows who treat them badly.</p>
                </div>
                <div class="arct-2">
                    <img class="rounded px-4 pb-4 float-right" src="{{asset('images/bocikk2.jpg')}}">
                    <p class="arct-text text-left p-4">Research has shown that cows clearly understand cause-and - effect relationships - a sure sign of advanced cognitive abilities. For example, they can learn how to push a lever to operate a drinking fountain when they’re thirsty or to press a button with their heads to release grain when they’re hungry. Researchers have found that cows not only can figure out problems but also enjoy the intellectual challenge and get excited when they find a solution, just as humans do.</p>
                </div>
                <div class="arct-3">
                    <img class="rounded p-4 float-left" src="{{asset('images/bocikk3.jpg')}}">
                    <p class="arct-text text-right p-4">A herd of cows is very much like a pack of wolves, with alpha animals and complex social dynamics. Each animal can recognize 50 or more members of the herd, and relationships are very important to cows. They consistently choose leaders who have good social skills and are intelligent, inquisitive, self-confident, and experienced—while pushiness, selfishness, a large size, and brawniness are not recognized as suitable leadership qualities.</p>
                </div>
            </div>
        </div>

        <div class="col m-4 bg-white ">
            <div class="list-group list-group-flush text-left py-4">
                <a href="#" class="list-group-item list-group-item-action">Taxonomy</a>
                <a href="#" class="list-group-item list-group-item-action">Etimology</a>
                <a href="#" class="list-group-item list-group-item-action">Terminology</a>
                <a href="#" class="list-group-item list-group-item-action">Anatomy</a>
                <a href="#" class="list-group-item list-group-item-action">Behavior</a>
                <a href="#" class="list-group-item list-group-item-action">Genetics</a>
                <a href="#" class="list-group-item list-group-item-action">Economy</a>
            </div>
            <div class="container py-3">
                <a href="#dem" class="btn btn-outline-light text-dark" data-toggle="collapse">Learn More</a>
                <div id="dem" class="collapse py-3">
                    Around 10,500 years ago, cattle were domesticated from as few as 80 progenitors in central Anatolia, the Levant and Western Iran. According to an estimate from 2011, there are 1.4 billion cattle in the world.In 2009, cattle became one of the first livestock animals to have a fully mapped genome.Some consider cattle the oldest form of wealth, and cattle raiding consequently one of the earliest forms of theft.
                </div>
            </div>
        </div>
        <div></div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
