<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> COW. </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/hw.css')}}">
    <link href="https://fonts.googleapis.com/css?
        family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light sticky-top">
    <a href="hw.blade.php" class="navbar-brand"> COW. </a>
    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarMenu">
        <ul class="navbar-nav text-black-50">
            <li class="nav-item active">
                <a href="{{ route('hw') }}" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('fun') }}" class="nav-link">Fun</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('about') }}" class="nav-link">About</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('signup') }}" class="nav-link">Sign Up</a>
            </li>
        </ul>
    </div>
    <form class="form-inline pr-2">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
</nav>


<div id="slides" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#slides" data-slide-to="0" class="active"></li>
        <li data-target="#slides" data-slide-to="1"></li>
        <li data-target="#slides" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
        <div class="carousel-caption">
            <h1 class="display-2 font-weight-bold"> COWS. </h1>
            <h3>Cows are amongst the gentlest of breathing creatures; none show more passionate tenderness to their young when deprived of them; and, in short, I am not ashamed to profess a deep love for these quiet creatures.</h3>
            <h5>Thomas De Quincey</h5>
        </div>
        <div class="carousel-item active">
            <img src="{{asset('images/boci2.jpg')}}">
        </div>
        <div class="carousel-item">
            <img src="{{asset('images/boci5jo.jpg')}}">
        </div>
        <div class="carousel-item">
            <img src="{{asset('images/bocijo.jpg')}}">
        </div>
    </div>
</div>

<div class="container-fluid px-0 bg-secondary text-gray-50">
    <div class="jumbotron">
        <p class="lead text-center">Cows are as diverse as cats, dogs, and humans: Some are very quick learners, while others are a little slower. Some are bold and adventurous, while others are shy and timid. Some are friendly and considerate, while others are bossy and devious.</p>
    </div>
</div>
<div class="row pt-3 pb-5">
    <div class="card-group px-4 text-center">
        <div class="col-sm-4 py-3">
            <div class="card h-100">
                <div class="card-img-wrap"><img class="card-img-top" src="{{asset('images/kor.jpg')}}" alt="Card image cap"></div>
                <div class="card-body">
                    <h4 class="card-title">Cud chewing</h4>
                    <p class="card-text">While the animal is feeding, the food is swallowed without being chewed and goes into the rumen for storage until the animal can find a quiet place to continue the digestion process.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 py-3">
            <div class="card h-100">
                <div class="card-img-wrap"><img class="card-img-top" src="{{asset('images/card2.jpg')}}" alt="Card image cap"></div>
                <div class="card-body">
                    <h4 class="card-title">Hindu traditions</h4>
                    <p class="card-text">Cattle are venerated within the Hindu religion of India,according to the Mahabharata, they are to be treated with the same respect 'as one's mother'.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 py-3">
            <div class="card h-100">
                <div class="card-img-wrap"><img class="card-img-top" src="{{asset('images/card3.jpg')}}" alt="Card image cap"></div>
                <div class="card-body">
                    <h4 class="card-title">Highland cattle</h4>
                    <p class="card-text">Many people call them "hairy cows" or "fluffy cows" due to their thick coats.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="page-footer font-small teal pt-4 bg-light text-secondary">
    <div class="container-fluid text-center text-md-left">
        <div class="row text-center">
            <div class="col-md-4 mt-md-0 mt-3">
                <h4 class="text-uppercase font-weight-bold"><a class="text-secondary text-decoration-none"href="{{ route('hw') }}">HOME</a></h4>
                <hr class="rgba-white-light" style="margin: 0 15%;">
                <p class="py-4">Cows today are completely domesticated animals and don’t live in the wild. The original wild cow Auroch (Bos taurus primigenus) is now extinct. It was originally found in areas of Europe, Asia, and North Africa. Now, only the domesticated breeds are still alive.</p>
            </div>
            <div class="col-md-4 mb-md-0 mb-3">
                <h4 class="text-uppercase font-weight-bold"><a class="text-secondary text-decoration-none" href="{{ route('fun') }}">FUN</a></h4>
                <hr class="rgba-white-light" style="margin: 0 15%;">
                <p class="py-4">What are grumpy cows called?
                </p>
                <p class="pt-4" >Moody.</p>
            </div>
            <div class="col-md-4 mt-md-0 mt-3">
                <h4 class="text-uppercase font-weight-bold"><a class="text-secondary text-decoration-none"  href="{{ route('about') }}">ABOUT</a></h4>
                <hr class="rgba-white-light" style="margin: 0 15%;">
                <p class="py-4">Animal behaviorists have found that they interact in socially complex ways, developing friendships over time and sometimes holding grudges against other cows who treat them badly.</p>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
